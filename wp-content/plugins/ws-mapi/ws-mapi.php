<?php
/*
Plugin Name: Mapi WS
Description: WebServices pour la récupération des données de wordpress dans angular
Version: 1.2
Author: Florian BLARY, SQLI
Author URI: http://www.sqli.com/
*/

foreach (glob(__DIR__."/includes/*.php") as $file) {
    require_once $file;
}

foreach (glob(__DIR__."/ws-services/*.php") as $file) {
    require_once $file;
}
