<?php

/**
 * Mapi WS
 *
 * @package  WS_Mapi
 * @author   Florian BLARY <fblary@sqli.com>
 */
class Mapi_WS  {
    const ERROR_404_MESSAGE = 'Item or user not found';
    const ERROR_404_EXERCICE = 'Exercice not found';
    const ERROR_401_MESSAGE = 'You do not have permissions to view this data';
    const ERROR_401_LOGIN = 'Access denied';
    const ERROR_401_ACCOUNT = 'Votre compte n\'est pas actif';
    const ERROR_LOGIN_INUSE = 'Login already exists';
    const ERROR_EMAIL_INUSE = 'Email address already exists';
    const ERROR_500 = 'Server error';
    const ERROR_FORMAT = 'Format error';
}
