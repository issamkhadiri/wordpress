<?php
add_action( 'rest_api_init', function () {
    $controller = new About_WS();
    $controller->register_routes();
} );

/**
 * Post WS
 *
 * @package  WS_Mapi
 * @author   Florian BLARY <fblary@sqli.com>
 */
class About_WS extends WP_REST_Controller {
    /**
     * WS routes for abouts
     */
    public function register_routes() {
        $namespace = 'v1';
        $base = 'about';

        // exemple de route : http://wp.mapi-ng.localhost/wp-json/v1/abouts
        register_rest_route( $namespace, '/' . $base .'s', array(
            array(
                'methods'         => 'GET',
                'callback'        => array( $this, 'get_abouts' ),
                'permission_callback' => array( $this, 'permissions_check' )
            )
        ) );

        // exemple de route : http://wp.mapi-ng.localhost/wp-json/v1/about/
        register_rest_route( $namespace, '/' . $base . '/(?P<post>[a-zA-Z0-9-]+)', array(
            array(
                'methods'         => 'GET',
                'callback'        => array( $this, 'get_about' ),
                'permission_callback' => array( $this, 'permissions_check' )
            )
        ) );
    }

    /**
     * @param $request
     * @return WP_Error|WP_REST_Response
     */
    public function get_abouts( $request ) {
        $args = array(
            'numberposts' => -1,
            'category' => 0,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'include' => '',
            'exclude' => '',
            'meta_key' => '',
            'meta_value' =>'',
            'post_type' => 'sqli_about',
            'post_status' => 'publish',
            'suppress_filters' => true
        );

        $posts = wp_get_recent_posts( $args );

        if ( $posts ) {
            return new WP_REST_Response( AboutFormat::formatAbouts($posts), 200 );
        } else {
            return new WP_Error( 'code', Mapi_WS::ERROR_404_MESSAGE, array( 'status' => 404 ));
        }
    }

    /**
     * @param $request
     * @return WP_Error|WP_REST_Response
     */
    public function get_about( $request ) {
        $params = $request->get_params();
        $post = $params["post"];

        $args = array(
            'name' => $post,
            'post_type' =>'sqli_about',
            'post_status' =>'Publish',
            'numberposts' => 1
        );
        $posts = get_posts($args);
        if ( $posts ) {
            $results = AboutFormat::formatAbouts($posts, 'full');
            return new WP_REST_Response( $results[0], 200 );
        } else {
            return new WP_Error( 'code', Mapi_WS::ERROR_404_MESSAGE, array( 'status' => 404 ));
        }
    }

    /**
     * @param $request
     * @return bool|WP_Error²
     */
    public function permissions_check( $request ) {
        $permission =  new Permission_WS();
        return $permission->permissions_check( $request );
    }

    /**
     * @param $request
     * @return bool|WP_Error
     */
    public function permissions_user_check( $request ) {
        $permission =  new Permission_WS();
        return $permission->permissions_user_check( $request );
    }
}
