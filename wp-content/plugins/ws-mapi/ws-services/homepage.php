<?php
add_action( 'rest_api_init', function () {
    $controller = new Homepage_WS();
    $controller->register_routes();
} );

/**
 * Homepage WS
 *
 * @package  WS_Mapi
 * @author   Florian BLARY <fblary@sqli.com>
 */
class Homepage_WS extends WP_REST_Controller {
    /**
     * WS routes for homepage data
     */
    public function register_routes() {
        $namespace = 'v1';
        $base = 'homepage';

        // exemple de route : http://wp.mapi-ng.localhost/wp-json/v1/homepage
        register_rest_route( $namespace, '/' . $base , array(
            array(
                'methods'         => 'GET',
                'callback'        => array( $this, 'get_homepage_data' ),
                'permission_callback' => array( $this, 'permissions_check' )
            )
        ) );
    }

    /**
     * @param $request
     * @return WP_Error|WP_REST_Response
     */
    public function get_homepage_data( $request ) {
        // last 3 posts
        $args = array(
            'numberposts' => 3,
            'category' => 0,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'include' => '',
            'exclude' => '',
            'meta_key' => '',
            'meta_value' =>'',
            'post_type' => 'post',
            'post_status' => 'publish',
            'suppress_filters' => true
        );
        $posts = wp_get_recent_posts( $args );

        // all partners
        $args = array(
            'numberposts' => -1,
            'category' => 0,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'include' => '',
            'exclude' => '',
            'meta_key' => '',
            'meta_value' =>'',
            'post_type' => 'sqli_partner',
            'post_status' => 'publish',
            'suppress_filters' => true
        );
        $partners = wp_get_recent_posts( $args );

        // 2 last services
        $args = array(
            'numberposts' => 2,
            'category' => 0,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'include' => '',
            'exclude' => '',
            'meta_key' => '',
            'meta_value' =>'',
            'post_type' => 'sqli_service',
            'post_status' => 'publish',
            'suppress_filters' => true
        );
        $services = wp_get_recent_posts( $args );


        return new WP_REST_Response(
            [
                'posts' => PostFormat::formatPosts($posts),
                'partners' => PartnerFormat::formatPosts($partners),
                'services' => StandardFormat::formatPosts($services),
                'database' => [
                    'proqolid_logo_mini'
                    => ImageFormat::getImagesFromId(get_option('subscriptions_proqolid_logo_mini')),
                    'prolabels_logo_mini'
                    => ImageFormat::getImagesFromId(get_option('subscriptions_prolabel_logo_mini')),
                    'proinsight_logo_mini'
                    => ImageFormat::getImagesFromId(get_option('subscriptions_proinsight_logo_mini'))
                ]
            ]
            , 200 );
    }

    /**
     * @param $request
     * @return bool|WP_Error²
     */
    public function permissions_check( $request ) {
        $permission =  new Permission_WS();
        return $permission->permissions_check( $request );
    }

    /**
     * @param $request
     * @return bool|WP_Error
     */
    public function permissions_user_check( $request ) {
        $permission =  new Permission_WS();
        return $permission->permissions_user_check( $request );
    }
}
