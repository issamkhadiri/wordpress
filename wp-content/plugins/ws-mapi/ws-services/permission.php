<?php

/**
 * Permission WS
 *
 * @package  WS_Mapi
 * @author   Florian BLARY <fblary@sqli.com>
 */
class Permission_WS  {
    /**
     * Check key
     *
     * @param $request
     * @return bool|WP_Error
     */
    public function permissions_check( $request ) {
        if ( $request->get_header('key') != get_option('ws_secret_key') ) {
            return new WP_Error( 'rest_forbidden', Mapi_WS::ERROR_401_MESSAGE, array( 'status' => 401 ) );
        }

        return true;
    }

    /**
     * Check key and user existance
     *
     * @param $request
     * @return bool|WP_Error
     */
    public function permissions_user_check( $request ) {
        $userToken = $request->get_header('usertoken');

        $userId = UserFormat::getIdFromToken($userToken);

        if ( $request->get_header('key') != get_option('ws_secret_key') || !$userId ) {
            return new WP_Error( 'rest_forbidden', Mapi_WS::ERROR_401_MESSAGE, array( 'status' => 401 ) );
        }

        return true;
    }
}
