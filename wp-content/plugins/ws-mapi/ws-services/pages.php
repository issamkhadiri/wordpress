<?php
add_action('rest_api_init', function () {
    $controller = new Page_WS();
    $controller->register_routes();
});

/**
 * Post WS
 *
 * @package  WS_Mapi
 * @author   Florian BLARY <fblary@sqli.com>
 * @author   Julien Grandgirard <jgrandgirard@sqli.com>
 */
class Page_WS extends WP_REST_Controller
{
    /**
     * WS routes for exercises
     */
    public function register_routes()
    {
        $base = 'page';
        $namespace = 'v1';

        register_rest_route($base, '/subscriptions', array(
            array(
                'methods'         => 'GET',
                'callback'        => array( $this, 'get_subscriptions_page' ),
                'permission_callback' => array( $this, 'permissions_check' )
            )
        ));

        register_rest_route($base, '/estimation', array(
            array(
                'methods'         => 'GET',
                'callback'        => array( $this, 'get_estimation_page' ),
                'permission_callback' => array( $this, 'permissions_check' )
            )
        ));

        register_rest_route( $namespace, '/' . $base . '/(?P<page>[a-zA-Z0-9-]+)', array(
            array(
                'methods'         => 'GET',
                'callback'        => array( $this, 'get_one_page' ),
                'permission_callback' => array( $this, 'permissions_check' )
            )
        ) );
    }

    /**
     * WS pour récupérer les logos et description de la page
     *
     * @param $request
     * @return WP_Error|WP_REST_Response
     */
    public function get_subscriptions_page($request)
    {
        if (true) {
            return new WP_REST_Response([
                'subscriptions_proqolid_logo'
                    => ImageFormat::getImagesFromId(get_option('subscriptions_proqolid_logo')),
                'subscriptions_proqolid_description'
                    => get_option('subscriptions_proqolid_description'),
                'subscriptions_prolabel_logo'
                    => ImageFormat::getImagesFromId(get_option('subscriptions_prolabel_logo')),
                'subscriptions_prolabel_description'
                    => get_option('subscriptions_prolabel_description'),
                'subscriptions_proinsight_logo'
                    => ImageFormat::getImagesFromId(get_option('subscriptions_proinsight_logo')),
                'subscriptions_proinsight_description'
                    => get_option('subscriptions_proinsight_description'),
                'subscriptions_provide_logo'
                    => ImageFormat::getImagesFromId(get_option('subscriptions_provide_logo')),
                'subscriptions_provide_description'
                    => get_option('subscriptions_provide_description')
            ], 200);
        } else {
            return new WP_Error('code', Mapi_WS::ERROR_404_MESSAGE, array( 'status' => 404 ));
        }
    }

    /**
     * WS pour récupérer le logo de la page
     *
     * @param $request
     * @return WP_Error|WP_REST_Response
     */
    public function get_estimation_page($request)
    {
        if (true) {
            return new WP_REST_Response([
                'subscriptions_proqolid_logo'
                    => ImageFormat::getImagesFromId(get_option('subscriptions_proqolid_logo')),
                'subscriptions_proqolid_logo_mini'
                    => ImageFormat::getImagesFromId(get_option('subscriptions_proqolid_logo_mini')),
                'subscriptions_proqolid_logo_mini_blue'
                => ImageFormat::getImagesFromId(get_option('subscriptions_proqolid_logo_mini_blue')),
                'subscriptions_prolabel_logo'
                    => ImageFormat::getImagesFromId(get_option('subscriptions_prolabel_logo')),
                'subscriptions_prolabel_logo_mini'
                    => ImageFormat::getImagesFromId(get_option('subscriptions_prolabel_logo_mini')),
                'subscriptions_prolabel_logo_mini_blue'
                => ImageFormat::getImagesFromId(get_option('subscriptions_prolabel_logo_mini_blue')),
                'subscriptions_proinsight_logo'
                    => ImageFormat::getImagesFromId(get_option('subscriptions_proinsight_logo')),
                'subscriptions_proinsight_logo_mini'
                    => ImageFormat::getImagesFromId(get_option('subscriptions_proinsight_logo_mini')),
                'subscriptions_proinsight_logo_mini_blue'
                => ImageFormat::getImagesFromId(get_option('subscriptions_proinsight_logo_mini_blue')),
                'subscriptions_provide_logo'
                    => ImageFormat::getImagesFromId(get_option('subscriptions_provide_logo')),
                'subscriptions_provide_logo_mini'
                    => ImageFormat::getImagesFromId(get_option('subscriptions_provide_logo_mini')),
                'subscriptions_provide_logo_mini_blue'
                    => ImageFormat::getImagesFromId(get_option('subscriptions_provide_logo_mini_blue'))
            ], 200);
        } else {
            return new WP_Error('code', Mapi_WS::ERROR_404_MESSAGE, array( 'status' => 404 ));
        }
    }

    /**
     * WS pour récupérer les mentions légales
     * /wp-json/v1/page/page-d-exemple/en
     *
     * @param $request
     * @return WP_Error|WP_REST_Response
     */
    public function get_one_page( $request ) {
        $params = $request->get_params();
        $page = $params["page"];
        $id = get_id_by_slug($page);
        $page = get_posts(
            array(
                'include' => $id,
                'post_type' => 'page',
            )
        );

        if ( $page && $id ) {
            return new WP_REST_Response( [
                'slug' => $page[0]->post_name,
                'title' => $page[0]->post_title,
                'date' => $page[0]->post_date,
                'updatedate' => $page[0]->post_modified,
                'link' => $page[0]->guid,
                'html_content' => do_shortcode($page[0]->post_content),
                'visual' => ImageFormat::getImagesFromId(get_post_thumbnail_id($page[0]->ID))
            ], 200 );
        } else {
            return new WP_Error( 'code', Mapi_WS::ERROR_404_MESSAGE, array( 'status' => 404 ));
        }
    }

    /**
     * @param $request
     * @return bool|WP_Error
     */
    public function permissions_check($request)
    {
        $permission =  new Permission_WS();
        return $permission->permissions_check($request);
    }

    /**
     * @param $request
     * @return bool|WP_Error
     */
    public function permissions_user_check($request)
    {
        $permission =  new Permission_WS();
        return $permission->permissions_user_check($request);
    }
}
