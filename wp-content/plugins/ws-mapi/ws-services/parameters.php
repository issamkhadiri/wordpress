<?php
add_action('rest_api_init', function () {
    $controller = new Parameters_WS();
    $controller->register_routes();
});

/**
 * Post WS
 *
 * @package  WS_Mapi
 * @author   Sandy Cnigniet <scnigniet@sqli.com>
 */
class Parameters_WS extends WP_REST_Controller
{
    /**
     * WS for get the parameters
     */
    public function register_routes()
    {
        $base = 'parameters';

        register_rest_route($base, '/footer-social-networks', array(
            array(
                'methods'         => 'GET',
                'callback'        => array( $this, 'get_footer_social_network' ),
                'permission_callback' => array( $this, 'permissions_check' )
            )
        ));

        register_rest_route($base, '/footer-links', array(
            array(
                'methods'         => 'GET',
                'callback'        => array( $this, 'get_footer_links' ),
                'permission_callback' => array( $this, 'permissions_check' )
            )
        ));

        register_rest_route($base, '/catalog-page-infos', array(
            array(
                'methods'         => 'GET',
                'callback'        => array( $this, 'get_catalog_page_infos' ),
                'permission_callback' => array( $this, 'permissions_check' )
            )
        ));

    }

    /**
     * WS pour récupérer les lien reseaux sociaux du footer
     *
     * @param $request
     * @return WP_REST_Response
     */
    public function get_footer_social_network($request)
    {
        return new WP_REST_Response([
            'facebook' => get_option('facebook_link'),
            'twitter' => get_option('twitter_link'),
            'linkedin' => get_option('linkedin_link'),
        ], 200);
    }

    /**
     * WS pour récupérer les liens du footer
     *
     * @param $request
     * @return WP_REST_Response
     */
    public function get_footer_links($request)
    {
        $fields = new ArrayObject();
        for ($i = 1; $i <= 14; $i++) {
            $fields->append(['title' => get_option('title_'.$i), 'link' => get_option('link_'.$i)]);
        }
        return new WP_REST_Response($fields, 200);
    }

    /**
     * @param $request
     * @return bool|WP_Error
     */
    public function permissions_check($request)
    {
        $permission =  new Permission_WS();
        return $permission->permissions_check($request);
    }

    /**
     * @param $request
     * @return bool|WP_Error
     */
    public function permissions_user_check($request)
    {
        $permission =  new Permission_WS();
        return $permission->permissions_user_check($request);
    }

    /**
     * WS pour récupérer les logos et description de la page
     *
     * @param $request
     * @return WP_Error|WP_REST_Response
     */
    public function get_catalog_page_infos($request)
    {
        return new WP_REST_Response([
            'catalog_page_description' => get_option('catalog_page_description'),
        ], 200);
    }
}
