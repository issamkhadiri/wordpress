<?php
/**
 * @param $id
 * @param $key
 * @return bool
 */
function ws_get_post_meta($id, $key, $type = false) {
    $data = get_post_meta($id, $key);

    if($data) {
        if(isset($data[0])) {
            return $data[0];
        }
    }

    if($type == 'string') {
        return '';
    }
    return 0;
}

/**
 * @return array
 */
function get_user_admin_ids() {
    $adminIds = array();
    $admins = get_users(array('role' => 'administrator'));

    foreach ($admins as $admin) {
        array_push($adminIds, $admin->ID);
    }

    return $adminIds;
}

/**
 * @param $request
 * @return array
 */
function get_eligible_users_ids($request) {
    $ids = get_user_admin_ids();

    $userToken = $request->get_header('usertoken');
    $userId = (int) UserFormat::getIdFromToken($userToken);

    array_push($ids, $userId);

    return $ids;
}

function get_id_by_slug($page_slug) {
    $page = get_page_by_path($page_slug);
    if ($page) {
        return $page->ID;
    } else {
        return null;
    }
}
