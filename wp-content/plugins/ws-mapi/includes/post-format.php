<?php
/**
 * Post formater
 *
 * @package  WS_Mapi
 * @author   Florian BLARY <fblary@sqli.com>
 */
Class PostFormat {
    /**
     * Parse one post
     *
     * @param $post
     * @return array
     */
    static public function formatPost($post) {
        if(!is_array($post)) {
            $post = (array) $post;
        }

        $category = get_the_category($post['ID']);
        $category = $category[0];
        $post_meta = get_post_meta($post['ID']);
        return [
            'code' => $post['ID'],
            'title' => $post['post_title'],
            'slug' => $post['post_name'],
            'description' => get_the_excerpt($post['ID']),
            'html_content' => do_shortcode($post['post_content']),
            'date' => $post['post_date'],
            'updatedate' => $post['post_modified'],
            'category' => $category->slug,
            'link' => $post['guid'],
            'visual' => ImageFormat::getImagesFromId(get_post_thumbnail_id($post['ID'])),
            'home_page_image_link' => $post_meta['home_page_image_link'][0]
        ];
    }

    /**
     * Get multiple posts
     *
     * @param $posts
     * @return array
     */
    static public function formatPosts($posts) {
        $data = [];

        foreach ($posts as $post) {
            $data[] = self::formatPost($post);
        }

        return $data;
    }
}
