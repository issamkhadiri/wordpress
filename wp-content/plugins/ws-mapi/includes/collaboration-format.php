<?php
/**
 * Collaboration formater
 *
 * @package  WS_Mapi
 * @author   Florian Bouhamdani <fbouhamdani@sqli.com>
 */
Class CollaborationFormat {
    /**
     * Parse one collaboration
     *
     * @param $post
     * @param $format
     * @return array
     */
    static public function formatCollaboration($post, $format = 'light') {
        if(!is_array($post)) {
            $post = (array) $post;
        }

        $categorySlug = null;
        $categoryName = null;

        $data = [
            'title' => $post['post_title'],
            'slug' => $post['post_name'],
            'category_slug' => $categorySlug,
        ];

        $adddata = [
            'code' => $post['ID'],
            'title' => $post['post_title'],
            'category_name' => $categoryName,
            'description' => get_the_excerpt($post['ID']),
            'html_content' => do_shortcode($post['post_content']),
            'date' => $post['post_date'],
            'updatedate' => $post['post_modified'],
            'link' => $post['guid'],
            'visual' => ImageFormat::getImagesFromId(get_post_thumbnail_id($post['ID'])),
        ];

        if($format == 'light') {
            return $data;
        } else {
            return array_merge($data, $adddata);
        }
    }

    /**
     * Get multiple collaborations
     *
     * @param $posts
     * @param $format
     * @return array
     */
    static public function formatCollaborations($posts, $format = 'light') {
        $data = [];

        foreach ($posts as $post) {
            $data[] = self::formatCollaboration($post, $format);
        }

        return $data;
    }
}
