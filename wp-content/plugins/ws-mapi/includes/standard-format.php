<?php
/**
 * Standard post formater
 *
 * @package  WS_Mapi
 * @author   Florian BLARY <fblary@sqli.com>
 */
Class StandardFormat {
    /**
     * Parse one standard post
     *
     * @param $post
     * @return array
     */
    static public function formatPost($post) {
        if(!is_array($post)) {
            $post = (array) $post;
        }

        return [
            'code' => $post['ID'],
            'title' => $post['post_title'],
            'slug' => $post['post_name'],
            'description' => get_the_excerpt($post['ID']),
            'html_content' => do_shortcode($post['post_content']),
            'visual' => ImageFormat::getImagesFromId(get_post_thumbnail_id($post['ID'])),
        ];
    }

    /**
     * Get multiple standard posts
     *
     * @param $posts
     * @return array
     */
    static public function formatPosts($posts) {
        $data = [];

        foreach ($posts as $post) {
            $data[] = self::formatPost($post);
        }

        return $data;
    }
}
