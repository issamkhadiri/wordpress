<?php
/**
 * Partner formater
 *
 * @package  WS_Mapi
 * @author   Florian BLARY <fblary@sqli.com>
 */
Class PartnerFormat {
    /**
     * Parse one partner
     *
     * @param $post
     * @return array
     */
    static public function formatPost($post) {
        if(!is_array($post)) {
            $post = (array) $post;
        }

        return [
            'code' => $post['ID'],
            'title' => $post['post_title'],
            'slug' => $post['post_name'],
            'link' => get_post_meta($post['ID'], 'sqli_partner_link', true),
            'visual' => ImageFormat::getImagesFromId(get_post_thumbnail_id($post['ID'])),
        ];
    }

    /**
     * Get multiple partners
     *
     * @param $posts
     * @return array
     */
    static public function formatPosts($posts) {
        $data = [];

        foreach ($posts as $post) {
            $data[] = self::formatPost($post);
        }

        return $data;
    }
}
