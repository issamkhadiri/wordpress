<?php
/**
 * Image formater
 *
 * @package  WS_Mapi
 * @author   Florian BLARY <fblary@sqli.com>
 */
Class ImageFormat {
    /**
     * @param $images
     * @return array
     */
    static function parseImages($images) {
        $data = [];
        $i = 1;
        if (is_array($images)) {
            foreach ($images as $image) {
                $data[] = [
                    'order' => $i,
                    'visual' => self::getImagesFromId($image)
                ];
                $i++;
            }
        }
        return $data;
    }

    /**
     * Return images array from id/url
     *
     * @param $id
     * @return array
     */
    static function getImagesFromId($id) {
        // check if it's already a url
        if (strpos($id, 'http') !== false) {
            $id = attachment_url_to_postid($id);
        }

        $images = [];
        $sizes = ['thumbnail', 'medium', 'medium_large', 'large'];
        foreach ($sizes as $size) {
            $images[$size] = wp_get_attachment_image_src($id, $size)[0];
            $images[$size.'-width'] = wp_get_attachment_image_src($id, $size)[1];
            $images[$size.'-height'] = wp_get_attachment_image_src($id, $size)[2];
        }

        return $images;
    }
}
