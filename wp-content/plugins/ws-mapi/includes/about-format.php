<?php
/**
 * About formater
 *
 * @package  WS_Mapi
 * @author   Florian BLARY <fblary@sqli.com>
 */
Class AboutFormat {
    /**
     * Parse one about
     *
     * @param $post
     * @param $format
     * @return array
     */
    static public function formatAbout($post, $format = 'light') {
        if(!is_array($post)) {
            $post = (array) $post;
        }

        $categories = wp_get_post_terms($post['ID'], ['about_categories']);

        if($categories) {
            $category = $categories[0];
            $categorySlug = $category->slug;
            $categoryName = $category->name;
        } else {
            $categorySlug = null;
            $categoryName = null;
        }

        $data = [
            'title' => $post['post_title'],
            'slug' => $post['post_name'],
            'category_slug' => $categorySlug,
        ];

        $adddata = [
            'code' => $post['ID'],
            'title' => $post['post_title'],
            'category_name' => $categoryName,
            'description' => get_the_excerpt($post['ID']),
            'html_content' => do_shortcode($post['post_content']),
            'date' => $post['post_date'],
            'updatedate' => $post['post_modified'],
            'link' => $post['guid'],
            'visual' => ImageFormat::getImagesFromId(get_post_thumbnail_id($post['ID'])),
        ];

        if($format == 'light') {
            return $data;
        } else {
            return array_merge($data, $adddata);
        }
    }

    /**
     * Get multiple abouts
     *
     * @param $posts
     * @param $format
     * @return array
     */
    static public function formatAbouts($posts, $format = 'light') {
        $data = [];

        foreach ($posts as $post) {
            $data[] = self::formatAbout($post, $format);
        }

        return $data;
    }
}
