<?php
/**
 * @package    SqliMapiTheme
 * @author     fblary <fblary@sqli.com>
 */

add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 1568, 9999 );

require get_template_directory() . '/inc/nocomments.php';
require get_template_directory() . '/inc/sqli-callbacks.php';
require get_template_directory() . '/inc/sqli-theme.php';
require get_template_directory() . '/inc/contenttype/sqli-about.php';
require get_template_directory() . '/inc/contenttype/sqli-collaboration.php';
require get_template_directory() . '/inc/contenttype/sqli-partner.php';
require get_template_directory() . '/inc/contenttype/sqli-service.php';
require get_template_directory() . '/inc/contenttype/sqli-slider.php';

// disable gutenberg
//add_filter('use_block_editor_for_post', '__return_false');
