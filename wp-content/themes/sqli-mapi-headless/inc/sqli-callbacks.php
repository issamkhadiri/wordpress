<?php
/**
 * @package    SqliMapiTheme
 * @author     fblary <fblary@sqli.com>
 */

/**
 * Callback pour input de type texte
 * @param $data
 */
function text_callback($data){
    ?>
    <input name="<?php echo $data['id']?>" type="text" id="<?php echo $data['id']?>" value="<?php echo esc_attr( get_option($data['id']) ); ?>">
    <?php if($data['label']) {?><p class="description"><?php echo $data['label']?></p><?php }?>
    <?php
}

/**
 * Callback pour input de type textarea
 * @param $data
 */
function textarea_callback($data){
    ?>
    <textarea id="<?php echo $data['id']?>" name="<?php echo $data['id']?>" class="materialize-textarea"><?php echo esc_attr( get_option($data['id']) ); ?></textarea>
    <?php
}

/**
 * Callback pour input de type checkbox
 * @param $data
 */
function checkbox_callback($data){
    ?>
    <input id="<?php echo $data['id']?>" name="<?php echo $data['id']?>" type="checkbox" value="1" <?php checked( get_option($data['id']), 1, true ); ?>>
    <label for="<?php echo $data['id']?>"><?php echo $data['label']?></label>
    <?php
}

/**
 * Callback pour input de type colorpicker
 * @param $data
 */
function colorpicker_callback($data){
    ?>
    <input class="jscolor" name="<?php echo $data['id']?>" id="<?php echo $data['id']?>" value="<?php echo esc_attr( get_option($data['id']) ); ?>">
    <?php if($data['label']) {?><p class="description"><?php echo $data['label']?></p><?php }?>
    <?php
}

/**
 * Callback pour input de type selecteur range
 * @param $data
 */
function range_callback($data){
    ?>
    <p class="range-field">
        <input type="range" name="<?php echo $data['id']?>" id="<?php echo $data['id']?>" value="<?php echo esc_attr( get_option($data['id']) ); ?>" min='0' max='1' step='.01'/>
    </p>
    <?php
}

/**
 * Callback pour input de type selecteur switch
 * @param $data
 */
function switch_callback($data){
    ?>
    <div class="switch">
        <label>
            Off
            <input type="checkbox" name="<?php echo $data['id']?>" id="<?php echo $data['id']?>" value="1" <?php checked( get_option($data['id']), 1, true ); ?>>
            <span class="lever"></span>
            On
        </label>
    </div>
    <?php
}

/**
 * Callback pour input de type image
 * @param $data
 */
function image_callback($data) {
    wp_enqueue_media();
    ?>
    <div class='image-preview-wrapper' id="image-wrapper<?php echo $data['id']?>" style="margin-right:15px;float:left;<?php if(!get_option($data['id'])) {echo 'display:none';}?>">
        <img class="materialboxed" style="height:initial;width:200px;margin-bottom:10px" name="preview<?php echo $data['id']?>" id="preview<?php echo $data['id']?>" src='<?php echo wp_get_attachment_url( get_option($data['id']) ); ?>' height='100'>
    </div>
    <a data-callbackid="<?php echo $data['id']?>" style="float:left" class="upload_image_button btn waves-effect waves-light"><i class="material-icons">perm_media</i></a>
    <a id="reset-btn<?php echo $data['id']?>" data-callbackid="<?php echo $data['id']?>" style="margin-left:15px;float:left;<?php if(!get_option($data['id'])) {echo 'display:none';}?>" class="reset_image_button btn waves-effect waves-light red"><i class="material-icons">not_interested</i></a>
    <input type='hidden' name='<?php echo $data['id']?>' id='<?php echo $data['id']?>' value='<?php echo esc_attr( get_option($data['id']) ); ?>'>
    <?php
}

/**
 * Callback pour input de type radiobtn
 * @param $data
 */
function radiobtn_callback($data){
    foreach ($data['lines'] as $key => $line) {
        $checked = '';
        if(get_option($data['id']) == $key) {
            $checked = 'checked="checked"';
        }
        echo '<p>
          <input name="'.$data['id'].'" type="radio" value="'.$key.'" id="'.$data['id'].$line.'" '.$checked.'/>
          <label for="'.$data['id'].$line.'">'.$line.'</label>
        </p>';
    }
}

/**
 * Callback pour input de type select
 * @param $data
 */
function select_callback($data){
    ?>
    <select name="<?php echo $data['id']?>" id="<?php echo $data['id']?>">
        <?php foreach ($data['lines'] as $key => $line) {
            echo '<option value="'.$key.'" '.selected( get_option($data['id']), $key ).'>'.$line.'</option>';
        }
        ?>
    </select>
    <?php
}

/**
 * Callback pour input de type date
 * @param $data
 */
function date_callback($data){
    ?>
    <input type="date" id="<?php echo $data['id']?>" name="<?php echo $data['id']?>" value="<?php echo esc_attr( get_option($data['id']) ); ?>">
    <?php
}

/**
 * Select box for all pages
 * @param $data
 */
function pages_callback($data){
    ?>
    <select name="<?php echo $data['id']?>" id="<?php echo $data['id']?>">
        <?php
    $selected_page = get_option($data['id']);

    $pages = get_pages();
    foreach ( $pages as $page ) {
        $option = '<option value="' . $page->ID . '" ';
        $option .= ( $page->ID == $selected_page ) ? 'selected="selected"' : '';
        $option .= '>';
        $option .= $page->post_title;
        $option .= '</option>';
        echo $option;
    }

        ?>
    </select>
    <?php
}


/**
 * Script pour les différents callbacks
 */
add_action( 'admin_footer', 'sqli_callback_scripts' );
function sqli_callback_scripts() {

    ?><script type='text/javascript'>

        jQuery( document ).ready( function( $ ) {

            // Uploading files
            var file_frame;
            var wp_media_post_id = null;
            var set_to_post_id = null;

            jQuery('.reset_image_button').on('click', function( event ){
                window.callbackid = jQuery(this).data('callbackid');

                $( '#image-wrapper'+window.callbackid ).hide();
                $( '#'+window.callbackid ).val("");
                $( '#reset-btn'+window.callbackid ).hide();
            });

            jQuery('.upload_image_button').on('click', function( event ){

                window.callbackid = jQuery(this).data('callbackid');

                event.preventDefault();

                // If the media frame already exists, reopen it.
                if ( file_frame ) {
                    // Set the post ID to what we want
                    file_frame.uploader.uploader.param( 'post_id', set_to_post_id );
                    // Open frame
                    file_frame.open();
                    return;
                } else {
                    // Set the wp.media post id so the uploader grabs the ID we want when initialised
                    wp.media.model.settings.post.id = set_to_post_id;
                }

                // Create the media frame.
                file_frame = wp.media.frames.file_frame = wp.media({
                    title: 'Select a image to upload',
                    button: {
                        text: 'Use this image',
                    },
                    multiple: false	// Set to true to allow multiple files to be selected
                });

                // When an image is selected, run a callback.
                file_frame.on( 'select', function() {
                    // We set multiple to false so only get one image from the uploader
                    attachment = file_frame.state().get('selection').first().toJSON();

                    // Do something with attachment.id and/or attachment.url here
                    $( '#preview'+window.callbackid ).attr( 'src', attachment.url ).css( 'width', '200' );
                    $( '#image-wrapper'+window.callbackid ).show();
                    $( '#'+window.callbackid ).val( attachment.id );
                    $( '#reset-btn'+window.callbackid ).show();

                    // Restore the main post ID
                    wp.media.model.settings.post.id = wp_media_post_id;
                });

                // Finally, open the modal
                file_frame.open();
            });

            // Restore the main ID when the add media button is pressed
            jQuery( 'a.add_media' ).on( 'click', function() {
                wp.media.model.settings.post.id = wp_media_post_id;
            });
        });
    </script><?php
}

/**
 * Create form for WordPress
 * @param $options
 * @param $optionsId
 * @param $settingsId
 */
function sqliBindOptions($options, $optionsId, $settingsId) {
    add_settings_section($settingsId, false, false, $optionsId);

    foreach($options as $key => $option) {
        if(isset($option['label'])) {
            $label = $option['label'];
        } else {
            $label = false;
        }

        if(!isset($option['text'])) {
            $option['text'] = 'Default text';
        }

        switch ($option['type']) {
            case "text":
                add_settings_field( $key, $option['text'], "text_callback", $optionsId, $settingsId, array('id' => $key, 'label' => $label) );
                break;
            case "textarea":
                add_settings_field( $key, $option['text'], "textarea_callback", $optionsId, $settingsId, array('id' => $key, 'label' => $label) );
                break;
            case "radio":
                add_settings_field( $key, $option['text'], "radiobtn_callback", $optionsId, $settingsId, array('id' => $key, 'label' => $label, 'lines' => $option['lines']) );
                break;
            case "select":
                add_settings_field( $key, $option['text'], "select_callback", $optionsId, $settingsId, array('id' => $key, 'label' => $label, 'lines' => $option['lines']) );
                break;
			case "pages":
                add_settings_field( $key, $option['text'], "pages_callback", $optionsId, $settingsId, array('id' => $key, 'label' => $label) );
                break;
            case "checkbox":
                add_settings_field( $key, $option['text'], "checkbox_callback", $optionsId, $settingsId, array('id' => $key, 'label' => $label) );
                break;
            case "colorpicker":
                add_settings_field( $key, $option['text'], "colorpicker_callback", $optionsId, $settingsId, array('id' => $key, 'label' => $label) );
                break;
            case "range":
                add_settings_field( $key, $option['text'], "range_callback", $optionsId, $settingsId, array('id' => $key, 'label' => $label) );
                break;
            case "image":
                add_settings_field( $key, $option['text'], "image_callback", $optionsId, $settingsId, array('id' => $key, 'label' => $label) );
                break;
            case "switch":
                add_settings_field( $key, $option['text'], "switch_callback", $optionsId, $settingsId, array('id' => $key, 'label' => $label) );
                break;
            case "date":
                add_settings_field( $key, $option['text'], "date_callback", $optionsId, $settingsId, array('id' => $key, 'label' => $label) );
                break;
        }

        register_setting($settingsId, $key );
    }
}
