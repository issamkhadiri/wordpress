<?php
/**
 * Panneau de configuration du thème
 *
 * @package    SqliMapiTheme
 * @author     fblary <fblary@sqli.com>
 * @author     jgrandgirard <jgrandgirard@sqli.com>
 * @author     Sandy Cnigniet <scnigniet@sqli.com>
 */

function sqli_theme_options_menu() {
    add_menu_page('Paramètres', 'Mapi options', 'manage_options', 'sqli_theme_options_url', 'sqli_theme_options_page');
}
add_action('admin_menu', 'sqli_theme_options_menu');

//    http://materializecss.com/icons.html
function sqli_theme_options_page(){
    $templateUrl = get_stylesheet_directory_uri();
?>

    <link rel="stylesheet" href="<?php echo $templateUrl ?>/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://cdn.tiny.cloud/1/zrzzqnbricysb5ksv3acxfid40shkqmtlqhhu4avtds32x1c/tinymce/5/tinymce.min.js" referrerpolicy="origin"/></script>
    <script>tinymce.init({selector:'textarea'});</script>
    <script src="<?php echo $templateUrl ?>/js/vendor/materialize.min.js"></script>
    <script>
        jQuery( document ).ready( function( $ ) {
            $('select').material_select();
            $('.materialboxed').materialbox();
        });
    </script>
    <style>
        *:focus {-webkit-box-shadow:none;box-shadow:none !important;border:transparent;outline:0;}
        a:hover {color:#fff;}
        i.tiny {top:2px;position:relative;}
        .formcontent {background:#fcfcfc;}
        .formcontent .col{margin-bottom:15px;}
        input.jscolor {padding-left:12px;}
    </style>
    <div class="row" style="background: #fff; margin-top: 15px">
        <div class="col s12">
            <ul class="tabs">
                <li class="tab col s3"><a class="active" href="#tab1"><i class="material-icons tiny">edit</i> Databases Informations</a></li>
                <li class="tab col s3"><a href="#footerSocialNetwork"><i class="material-icons tiny">link</i> Footer Social Network</a></li>
                <li class="tab col s3"><a href="#footerLinks"><i class="material-icons tiny">link</i> Footer Links</a></li>
            </ul>
        </div>
        <div id="tab1" class="col s12 formcontent">
            <div class="col s12 l6">
                <form method="post" action="options.php#tab1" name="sqli_theme_settings">
                    <?php
                    settings_fields("sqli_theme_settings");
                    do_settings_sections("sqli_theme_options");
                    ?>
                    <a class="btn waves-effect waves-light" onclick="window.document.sqli_theme_settings.submit()">Save</a>
                </form>
            </div>
        </div>
        <div id="footerSocialNetwork" class="col s12 formcontent">
            <div class="col s12 l6">
                <form method="post" action="options.php#footerSocialNetwork" name="footer_social_network_settings">
                    <?php
                    settings_fields("footer_social_network_settings");
                    do_settings_sections("footer_social_network_options");
                    ?>
                    <a class="btn waves-effect waves-light" onclick="window.document.footer_social_network_settings.submit()">Save</a>
                </form>
            </div>
        </div>
        <div id="footerLinks" class="col s12 formcontent">
            <div class="col s12 l6">
                <form method="post" action="options.php#footerLinks" name="footer_links_settings">
                    <?php
                    settings_fields("footer_links_settings");
                    do_settings_sections("footer_links_options");
                    ?>
                    <a class="btn waves-effect waves-light" onclick="window.document.footer_links_settings.submit()">Save</a>
                </form>
            </div>
        </div>
    </div>
    <?php
}

// Onglet Subscriptions Page
function sqli_subscriptions_page(){
    $options = [
        'subscriptions_proqolid_logo' => ['type' => 'image', 'text' => 'Proqolid : Logo'],
        'subscriptions_proqolid_logo_mini' => ['type' => 'image', 'text' => 'Proqolid : Logo mini blanc'],
        'subscriptions_proqolid_logo_mini_blue' => ['type' => 'image', 'text' => 'Proqolid : Logo mini bleu'],
        'subscriptions_proqolid_description' => ['type' => 'textarea', 'text' => 'Proqolid : Description'],

        'subscriptions_prolabel_logo' => ['type' => 'image', 'text' => 'Prolabel : Logo'],
        'subscriptions_prolabel_logo_mini' => ['type' => 'image', 'text' => 'Prolabel :Logo mini'],
        'subscriptions_prolabel_logo_mini_blue' => ['type' => 'image', 'text' => 'Prolabel : Logo mini bleu'],
        'subscriptions_prolabel_description' => ['type' => 'textarea', 'text' => 'Description', 'image' => 'Prolabel :Description'],

        'subscriptions_proinsight_logo' => ['type' => 'image', 'text' => 'Proinsight : Logo'],
        'subscriptions_proinsight_logo_mini' => ['type' => 'image', 'text' => 'Proinsight : Logo mini blanc'],
        'subscriptions_proinsight_logo_mini_blue' => ['type' => 'image', 'text' => 'Proinsight : Logo mini bleu'],
        'subscriptions_proinsight_description' => ['type' => 'textarea', 'text' => 'Proinsight : Description'],

        'subscriptions_provide_logo' => ['type' => 'image', 'text' => 'Provide : Logo'],
        'subscriptions_provide_logo_mini' => ['type' => 'image', 'text' => 'Provide : Logo mini blanc'],
        'subscriptions_provide_logo_mini_blue' => ['type' => 'image', 'text' => 'Provide : Logo mini bleu'],
        'subscriptions_provide_description' => ['type' => 'textarea', 'text' => 'Provide : Description'],
        'catalog_page_description' => ['type' => 'textarea', 'text' => 'Catalog Page : Description']
    ];
    sqliBindOptions($options, 'sqli_theme_options', 'sqli_theme_settings');
}
add_action( 'admin_init', 'sqli_subscriptions_page' );

function footer_social_network(){
    $rs = [
        'facebook_link' => ['type' => 'text', 'text' => 'Facebook Link'],
        'twitter_link' => ['type' => 'text', 'text' => 'Twitter Link'],
        'linkedin_link' => ['type' => 'text', 'text' => 'Linkedin Link'],
        ];

    sqliBindOptions($rs, 'footer_social_network_options', 'footer_social_network_settings');
}
add_action( 'admin_init', 'footer_social_network' );

function footer_links(){

    for ($i = 1; $i <= 14; $i++) {
        $fields['title_'.$i] = ['type' => 'text', 'text' => 'Title '.$i];
        $fields['link_'.$i] = ['type' => 'text', 'text' => 'Link '.$i];
    }

    sqliBindOptions($fields, 'footer_links_options', 'footer_links_settings');
}
add_action( 'admin_init', 'footer_links' );

function test_sqli_options_fct(){
    $options = [];

    sqliBindOptions($options, 'test_sqli_options', 'test_sqli_settings');
}
add_action( 'admin_init', 'test_sqli_options_fct' );
