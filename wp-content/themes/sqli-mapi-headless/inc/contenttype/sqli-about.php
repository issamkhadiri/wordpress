<?php
/**
 * @package    SqliMapiTheme
 * @author     fblary <fblary@sqli.com>
 */

// add new content type
add_action( 'init', 'sqli_register_about', 20 );
function sqli_register_about() {
    $labels = array(
        'name' => _x( 'About', 'sqli_about','custom' ),
        'singular_name' => _x( 'About', 'sqli_about', 'custom' ),
        'add_new' => _x( 'Add new about page', 'sqli_about', 'custom' ),
        'add_new_item' => _x( 'Add new about page', 'sqli_about', 'custom' ),
        'edit_item' => _x( 'Edit and about page', 'sqli_about', 'custom' ),
        'new_item' => _x( 'Add new about page', 'sqli_about', 'custom' ),
        'view_item' => _x( 'View the about page', 'sqli_about', 'custom' ),
        'search_items' => _x( 'Search an about page', 'sqli_about', 'custom' ),
        'not_found' => _x( 'No about page', 'sqli_about', 'custom' ),
        'not_found_in_trash' => _x( 'No about page', 'sqli_about', 'custom' ),
        'parent_item_colon' => _x( 'Parent :', 'sqli_about', 'custom' ),
        'menu_name' => _x( 'About', 'sqli_about', 'custom' ),
    );

    $args = array(
        'labels'             => $labels,
        'menu_icon'          => 'dashicons-welcome-write-blog',
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'show_in_rest'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'about' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'taxonomies'         => array('about_categories'),
        'supports'           => array( 'title', 'editor', 'thumbnail')
    );

    register_post_type( 'sqli_about', $args );
}

// add new taxonomy
add_action( 'init', 'create_topics_hierarchical_taxonomy', 0 );
function create_topics_hierarchical_taxonomy() {
    $labels = array(
        'name' => _x( 'Categories', 'taxonomy general name' ),
        'singular_name' => _x( 'Category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Categories' ),
        'all_items' => __( 'All Categories' ),
        'parent_item' => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item' => __( 'Edit Category' ),
        'update_item' => __( 'Update Category' ),
        'add_new_item' => __( 'Add New Category' ),
        'new_item_name' => __( 'New Category Name' ),
        'menu_name' => __( 'Categories' ),
    );

    register_taxonomy('about_categories',array('sqli_about'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_rest'      => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'about_categories' ),
    ));
}
