<?php
/**
 * @package    SqliMapiTheme
 * @author     fblary <fblary@sqli.com>
 */

// add new content type
add_action( 'init', 'sqli_register_service', 20 );
function sqli_register_service() {
    $labels = array(
        'name' => _x( 'Services', 'sqli_service','custom' ),
        'singular_name' => _x( 'Service', 'sqli_service', 'custom' ),
        'add_new' => _x( 'Add new service', 'sqli_service', 'custom' ),
        'add_new_item' => _x( 'Add new service', 'sqli_service', 'custom' ),
        'edit_item' => _x( 'Edit and service', 'sqli_service', 'custom' ),
        'new_item' => _x( 'Add new service', 'sqli_service', 'custom' ),
        'view_item' => _x( 'View the service', 'sqli_service', 'custom' ),
        'search_items' => _x( 'Search an service', 'sqli_service', 'custom' ),
        'not_found' => _x( 'No service', 'sqli_service', 'custom' ),
        'not_found_in_trash' => _x( 'No service', 'sqli_service', 'custom' ),
        'parent_item_colon' => _x( 'Parent :', 'sqli_service', 'custom' ),
        'menu_name' => _x( 'Services', 'sqli_service', 'custom' ),
    );

    $args = array(
        'labels'             => $labels,
        'menu_icon'          => 'dashicons-book',
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'show_in_rest'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'services' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'taxonomies'         => array('service_categories'),
        'supports'           => array( 'title', 'editor', 'thumbnail')
    );

    register_post_type( 'sqli_service', $args );
}
