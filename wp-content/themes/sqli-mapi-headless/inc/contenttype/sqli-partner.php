<?php
/**
 * @package    SqliMapiTheme
 * @author     fblary <fblary@sqli.com>
 */

// add new content type
add_action( 'init', 'sqli_register_partner', 20 );
function sqli_register_partner() {
    $labels = array(
        'name' => _x( 'Partners', 'sqli_partner','custom' ),
        'singular_name' => _x( 'Partner', 'sqli_partner', 'custom' ),
        'add_new' => _x( 'Add new partner page', 'sqli_partner', 'custom' ),
        'add_new_item' => _x( 'Add new partner page', 'sqli_partner', 'custom' ),
        'edit_item' => _x( 'Edit and partner page', 'sqli_partner', 'custom' ),
        'new_item' => _x( 'Add new partner page', 'sqli_partner', 'custom' ),
        'view_item' => _x( 'View the partner page', 'sqli_partner', 'custom' ),
        'search_items' => _x( 'Search an partner page', 'sqli_partner', 'custom' ),
        'not_found' => _x( 'No partner page', 'sqli_partner', 'custom' ),
        'not_found_in_trash' => _x( 'No partner page', 'sqli_partner', 'custom' ),
        'parent_item_colon' => _x( 'Parent :', 'sqli_partner', 'custom' ),
        'menu_name' => _x( 'Partners', 'sqli_partner', 'custom' ),
    );

    $args = array(
        'labels'             => $labels,
        'menu_icon'          => 'dashicons-networking',
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'show_in_rest'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'partners' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'taxonomies'         => array('partner_categories'),
        'supports'           => array( 'title', 'thumbnail', 'editor')
    );

    register_post_type( 'sqli_partner', $args );
}

add_action( 'add_meta_boxes', 'sqli_meta_partner_box' );
function sqli_meta_partner_box()
{
    add_meta_box( 'sqli-detail-partner', 'Details', 'sqli_meta_partner_box_cb', 'sqli_partner', 'normal', 'high' );
}

function sqli_meta_partner_box_cb($post)
{
    $link = get_post_meta($post->ID,'sqli_partner_link',true);
    echo '<label for="sqli_partner_link">Partner\'s link : </label>';
    echo '<input id="sqli_partner_link" type="text" style="width:50%" name="sqli_partner_link" value="'.$link.'" /><br><br>';
}

add_action( 'save_post', 'sqli_meta_partner_save' );
function sqli_meta_partner_save($post_ID )
{
    if(isset($_POST['sqli_partner_link'])){
        update_post_meta($post_ID,'sqli_partner_link', esc_html($_POST['sqli_partner_link']));
    }
}
