<?php
/**
 * @package    SqliMapiTheme
 * @author     fblary <fblary@sqli.com>
 */

// add new content type
add_action( 'init', 'sqli_register_slider', 20 );
function sqli_register_slider() {
    $labels = array(
        'name' => _x( 'Sliders', 'sqli_slider','custom' ),
        'singular_name' => _x( 'Slider', 'sqli_slider', 'custom' ),
        'add_new' => _x( 'Add new slider', 'sqli_slider', 'custom' ),
        'add_new_item' => _x( 'Add new slider', 'sqli_slider', 'custom' ),
        'edit_item' => _x( 'Edit and slider', 'sqli_slider', 'custom' ),
        'new_item' => _x( 'Add new slider', 'sqli_slider', 'custom' ),
        'view_item' => _x( 'View the slider', 'sqli_slider', 'custom' ),
        'search_items' => _x( 'Search an slider', 'sqli_slider', 'custom' ),
        'not_found' => _x( 'No slider', 'sqli_slider', 'custom' ),
        'not_found_in_trash' => _x( 'No slider', 'sqli_slider', 'custom' ),
        'parent_item_colon' => _x( 'Parent :', 'sqli_slider', 'custom' ),
        'menu_name' => _x( 'Sliders', 'sqli_slider', 'custom' ),
    );

    $args = array(
        'labels'             => $labels,
        'menu_icon'          => 'dashicons-slides',
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'show_in_rest'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'sliders' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'taxonomies'         => array('slider_categories'),
        'supports'           => array( 'title', 'editor', 'thumbnail')
    );

    register_post_type( 'sqli_slider', $args );
}
