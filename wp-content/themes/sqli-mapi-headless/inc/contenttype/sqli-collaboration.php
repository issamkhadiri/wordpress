<?php
/**
 * @package    SqliMapiTheme
 * @author     fbouhamdani <fbouhamdani@sqli.com>
 */

// add new content type
add_action( 'init', 'sqli_register_collaboration', 20 );
function sqli_register_collaboration() {
    $labels = array(
        'name' => _x( 'Collaborations', 'sqli_collaboration','custom' ),
        'singular_name' => _x( 'Collaborations', 'sqli_collaboration', 'custom' ),
        'add_new' => _x( 'Add new collaboration page', 'sqli_collaboration', 'custom' ),
        'add_new_item' => _x( 'Add new collaboration page', 'sqli_collaboration', 'custom' ),
        'edit_item' => _x( 'Edit and collaboration page', 'sqli_collaboration', 'custom' ),
        'new_item' => _x( 'Add new collaboration page', 'sqli_collaboration', 'custom' ),
        'view_item' => _x( 'View the collaboration page', 'sqli_collaboration', 'custom' ),
        'search_items' => _x( 'Search an collaboration page', 'sqli_collaboration', 'custom' ),
        'not_found' => _x( 'No collaboration page', 'sqli_collaboration', 'custom' ),
        'not_found_in_trash' => _x( 'No collaboration page', 'sqli_collaboration', 'custom' ),
        'parent_item_colon' => _x( 'Parent :', 'sqli_collaboration', 'custom' ),
        'menu_name' => _x( 'Collaboration', 'sqli_collaboration', 'custom' ),
    );

    $args = array(
        'labels'             => $labels,
        'menu_icon'          => 'dashicons-welcome-write-blog',
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'show_in_rest'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'collaboration' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'taxonomies'         => array('collaboration_categories'),
        'supports'           => array( 'title', 'editor', 'thumbnail')
    );

    register_post_type( 'sqli_collaboration', $args );
}
