<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// Set environment URLs
$environments = array(
    'wp.mapi-ng.localhost' => 'dev',
    'eprovide-ec-ngqa.mapi-trust.org' => 'ngqa',
    'eprovide-ec-ngpp.mapi-trust.org' => 'ngpp',
    'eprovide-ec.mapi-trust.org' => 'prod',
);

define('ENV', $environments[$_SERVER['HTTP_HOST']] ?? 'dev');

// Exit if ENVIRONMENT is undefined
if (!defined('ENV')) {
    exit('No database configured for this host');
}

// Location of environment-specific configuration
$wp_db_config = 'wp-config/wp-db-' . ENV . '.php';

// Check to see if the configuration file for the environment exists
if (file_exists(__DIR__ . '/' . $wp_db_config)) {
    require_once($wp_db_config);
} else {
    // Exit if configuration file does not exist
    exit('No database configuration found for this host');
}

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
