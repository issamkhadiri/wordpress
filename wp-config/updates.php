<?php
$environments = array(
    // Ajouter la relation dans wp-config.php
    'eprovide-ec-ngqa.mapi-trust.org' => [
        'host' => 'eu-eprovit-d202',
        'user' => 'eprovide',
        'password' => 'odN88lJ5tI3l',
        'database' => 'eprovide_ng_qa',
        'file' => 'wordpress_dump.sql'
    ],
    'eprovide-ec-ngpp.mapi-trust.org' => [
        'host' => 'eu-eprovit-d203',
        'user' => 'eprovide',
        'password' => 'odN88lJ5tI3l',
        'database' => 'eprovide_ng_pp',
        'file' => 'wordpress_dump.sql'
    ],
);

$env = $environments[$_SERVER['HTTP_HOST']];
$conn = new mysqli($env['host'], $env['user'], $env['password'], $env['database']);

/* check connection */
if ($conn->connect_errno) {
    print_r("Connect failed: %s\r\n", $conn->connect_error);
    exit();
}
echo "Connected successfully\r\n";

try {
    $filename = $env['file'];
    $op_data = '';
    $lines = file($filename);
    foreach ($lines as $line)
    {
        if (substr($line, 0, 2) == '--' || substr($line, 0, 2) == '/*' || $line == '')//This IF Remove Comment Inside SQL FILE
        {
            continue;
        }
        $op_data .= $line;
        if (substr(trim($line), -1, 1) == ';')//Breack Line Upto ';' NEW QUERY
        {
            $op_data = trim($op_data);
            if ($conn->query($op_data) === true) {
                echo $op_data . PHP_EOL;
            } else {
                echo mysqli_errno($conn) . "\r\n";
                echo mysqli_error($conn) . "\r\n";
                exit;
            }
            $op_data = '';
        }
    }
} catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\r\n";
}
mysqli_close($link);
echo "Connection closed\r\n";
die();
